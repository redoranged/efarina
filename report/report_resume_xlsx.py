# -*- coding: utf-8 -*-

import logging, datetime, xlsxwriter, calendar, pytz
from dateutil.relativedelta import relativedelta

from odoo import models
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class LaporanResume(models.AbstractModel):
    _name = 'report.efarina.resume_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, order):
        biaya_ids = data['context'].get('biaya_ids')
        start_year = int(data['context'].get('start_year'))
        start_month = int(data['context'].get('start_month'))
        end_year = int(data['context'].get('end_year'))
        end_month = int(data['context'].get('end_month'))

        total_days_of_month = calendar.monthrange(end_year, end_month)[1]
        date_start = datetime.date(start_year, start_month, 1)
        date_end = datetime.date(end_year if end_month < 12 else end_year + 1, end_month if end_month < 12 else 1, 1)
        
        # _logger.info('****************************************')
        # _logger.info(date_end)
        # _logger.info('****************************************')

        # Style
        bold = workbook.add_format({'bold': True})
        money_format = workbook.add_format({'num_format': '"Rp." #,##0', 'border': 1})
        money_format_bold = workbook.add_format({'bold': True, 'num_format': '"Rp." #,##0', 'border': 1})
        date_format = workbook.add_format({'num_format': 'd mmmm yyyy', 'border': 1})
        border = workbook.add_format({'border': 1})
        bold_border = workbook.add_format({'bold': True, 'border': 1})

        # Variable
        bulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ]

        # Resume
        sheet = workbook.add_worksheet("Resume Piutang  {} {}".format(bulan[end_month-1], end_year))
        # Column Width Setup
        sheet.set_column(0, 2, 32) # A - C

        col = 0
        row = 0

        # Header
        sheet.write(row, col, "Resume Piutang {} {}".format(bulan[end_month-1], end_year), bold)
        row = row + 2
        sheet.write(row, col, "Kelas", bold_border)
        col = col + 1
        sheet.write(row, col, "Uraian Biaya", bold_border)
        col = col + 1
        sheet.write(row, col, "Jumlah", bold_border)

        col = 0
        row = row + 1
        for tingkatan in self.env['efarina.tingkatan'].search([('active', '=', True)]):
            sheet.write(row, col, tingkatan.name, bold_border)
            col = col + 1
            siswa_ids = self.env['efarina.siswa'].search([('tingkatan_id', '=', tingkatan.id)])
            first_row = row
            for kewajiban in tingkatan.kewajiban_ids:
                sheet.write(row, col, kewajiban.name, bold_border)
                col = col + 1
                total = 0
                for siswa_id in siswa_ids:
                    for x in siswa_id.kewajiban_ids:
                        if x.biaya_id.id == kewajiban.biaya_id.id:
                            if x.type == 'monthly':
                                if x.date_month_year != False and x.date_month_year <= date_end or x.date_month_year == False:
                                    total += x.sisa_harus_bayar
                            elif x.type == 'annualy':
                                if int(x.year) <= date_end.year:
                                    total += x.sisa_harus_bayar
                            else:
                                total += x.sisa_harus_bayar
                # total = sum(sum(siswa_id.kewajiban_ids.filtered(lambda x:  x.date_month_year != False and 
                #                                                         x.date_month_year < date_end and 
                #                                                         x.biaya_id.id == kewajiban.biaya_id.id 
                #                                                     or x.date_month_year == False and 
                #                                                         x.biaya_id.id == kewajiban.biaya_id.id).mapped('sisa_harus_bayar')) for siswa_id in siswa_ids)
                sheet.write(row, col, total, money_format)
                # sheet.write(row, col, sum(sum(kewajiban_siswa.sisa_harus_bayar) for kewajiban_siswa in (swa.kewajiban_ids.filtered(lambda x: x.biaya_id == kewajiban.biaya_id.id) for swa in siswa)), money_format)
                col = 1
                row = row + 1
            sheet.write(row, col, "Total", bold_border)
            col = col + 1
            sheet.write(row, col, "=SUM({}{}:{}{})".format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), money_format_bold)

            col = 0
            row = row + 1

        col = 0
        row = row + 2

        sheet.write(row, col, "Rincian Piutang Siswa", bold_border)
        col = col + 1
        sheet.write(row, col, "Jumlah", bold_border)
        # Rincian Piutang Siswa
        col = 0
        row = row + 1
        first_row = row
        biayas = self.env['efarina.biaya'].browse(biaya_ids)
        for biaya in biayas:
            sheet.write(row, col, biaya.name, bold_border)
            col = col + 1
            kewajiban_siswas = self.env['efarina.siswa.kewajiban'].search([('biaya_id', '=', biaya.id)])
            total = 0
            for x in kewajiban_siswas:
                if x.type == 'monthly':
                    if x.date_month_year != False and x.date_month_year <= date_end or x.date_month_year == False:
                        total += x.sisa_harus_bayar
                elif x.type == 'annualy':
                    if int(x.year) <= date_end.year:
                        total += x.sisa_harus_bayar
                else:
                    total += x.sisa_harus_bayar
                    
            sheet.write(row, col, total, money_format)
            col = 0
            row = row + 1

        sheet.write(row, col, "Total", bold_border)
        col = col + 1
        sheet.write(row, col, "=SUM({}{}:{}{})".format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), money_format_bold)

        # Rincian Pendapatan Siswa
        sheet = workbook.add_worksheet("Rincian Pendapatan Siswa")

        # Column Width Setup
        sheet.set_column(0, 0, 20) # A
        sheet.set_column(1, 2, 15) # B - C
        sheet.set_column(3, 3, 32) # C - D

        col = 0
        row = 0

        # Bayar Penuh
        # Header
        sheet.write(row, col, "Data Siswa yang Bayar Penuh:", bold)
        row = row + 1
        sheet.write(row, col, "Kelas", bold_border)
        col = col + 1
        sheet.write(row, col, "Jumlah Siswa", bold_border)
        col = col + 1
        sheet.write(row, col, "Biaya Per Bulan", bold_border)
        col = col + 1
        sheet.write(row, col, "Pendapatan Rutin Uang Sekolah", bold_border)
        col = col + 1

        col = 0
        row = row + 1
        first_row = row
        for tingkatan in self.env['efarina.tingkatan'].search([('active', '=', True)]):
            sheet.write(row, col, tingkatan.name, border)
            col = col + 1
            siswa = self.env['efarina.siswa'].search([('tingkatan_id', '=', tingkatan.id), ('status', '=', 'bayar_penuh')])
            sheet.write(row, col, len(siswa), border)
            col = col + 1
            kewajiban_ids = self.env['efarina.tingkatan.kewajiban'].search([('tingkatan_id', '=', tingkatan.id), ('type', '=', 'monthly')])
            sheet.write(row, col, sum([sum(kewajiban_id.mapped('jumlah')) for kewajiban_id in kewajiban_ids]), money_format)
            col = col + 1
            sheet.write(row, col, "={}{}*{}{}".format(xlsxwriter.utility.xl_col_to_name(col-2), row+1, xlsxwriter.utility.xl_col_to_name(col-1), row+1), money_format_bold)
            col = 0
            row = row + 1
        sheet.write(row, col, "Total", bold_border)
        col = col + 1
        sheet.write(row, col, "=SUM({}{}:{}{})".format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), bold_border)
        col = col + 1
        sheet.write(row, col, "", border)
        col = col + 1
        sheet.write(row, col, "=SUM({}{}:{}{})".format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), money_format_bold)

        col = 0
        row = row + 2

        # Tidak Bayar Penuh
        # Header
        sheet.write(row, col, "Data Siswa yang Tidak Bayar Penuh:", bold)
        row = row + 1
        sheet.write(row, col, "Kelas", bold_border)
        col = col + 1
        sheet.write(row, col, "Jumlah Siswa", bold_border)
        col = col + 1
        sheet.write(row, col, "Biaya Per Bulan", bold_border)
        col = col + 1
        sheet.write(row, col, "Pendapatan Rutin Uang Sekolah", bold_border)
        
        col = 0
        row = row + 1
        first_row = row
        for tingkatan in self.env['efarina.tingkatan'].search([('active', '=', True)]):
            sheet.write(row, col, tingkatan.name, border)
            col = col + 1
            siswa = self.env['efarina.siswa'].search([('tingkatan_id', '=', tingkatan.id), ('status', '=', 'tidak_bayar_penuh')])
            sheet.write(row, col, len(siswa), border)
            col = col + 1
            # kewajiban = self.env['efarina.tingkatan.kewajiban'].search([('tingkatan_id', '=', tingkatan.id), ('type', '=', 'monthly')])
            sheet.write(row, col, '', money_format)
            col = col + 1
            sheet.write(row, col, "={}{}*{}{}".format(xlsxwriter.utility.xl_col_to_name(col-2), row+1, xlsxwriter.utility.xl_col_to_name(col-1), row+1), money_format_bold)
            col = 0
            row = row + 1
        sheet.write(row, col, "Total", bold_border)
        col = col + 1
        sheet.write(row, col, "=SUM({}{}:{}{})".format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), bold_border)
        col = col + 1
        sheet.write(row, col, "", border)
        col = col + 1
        sheet.write(row, col, "=SUM({}{}:{}{})".format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), money_format_bold)

        col = 0
        row = row + 2

        # Tidak Bayar
        # Header
        sheet.write(row, col, "Data Siswa yang Tidak Bayar:", bold)
        row = row + 1
        sheet.write(row, col, "Kelas", bold_border)
        col = col + 1
        sheet.write(row, col, "Jumlah Siswa", bold_border)
        
        col = 0
        row = row + 1
        first_row = row
        for tingkatan in self.env['efarina.tingkatan'].search([('active', '=', True)]):
            sheet.write(row, col, tingkatan.name, border)
            col = col + 1
            siswa = self.env['efarina.siswa'].search([('tingkatan_id', '=', tingkatan.id), ('status', '=', 'tidak_bayar')])
            sheet.write(row, col, len(siswa), border)
            col = 0
            row = row + 1
        sheet.write(row, col, "Total", bold_border)
        col = col + 1
        sheet.write(row, col, "=SUM({}{}:{}{})".format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), bold_border)