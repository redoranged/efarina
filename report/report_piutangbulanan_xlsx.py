# -*- coding: utf-8 -*-

import logging, datetime, xlsxwriter, calendar, pytz
from dateutil.relativedelta import relativedelta

from odoo import models
from odoo.exceptions import UserError
from odoo.tools import date_utils

_logger = logging.getLogger(__name__)

class LaporanPiutangBulanan(models.AbstractModel):
    _name = 'report.efarina.piutangbulanan_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, order):
        
        year = int(data['context'].get('year'))
        month = int(data['context'].get('month'))

        # date_start = datetime.date(year, month, 1)
        # total_days_of_month = calendar.monthrange(year, month)[1]
        # date_end = datetime.date(year, month, total_days_of_month)
        
        # user_tz = pytz.timezone(data['context'].get('tz') or self.env.user.tz or pytz.utc)
        # start_utc_datetime = user_tz.localize(date_start).astimezone(pytz.utc)
        # end_utc_datetime = user_tz.localize(date_end).astimezone(pytz.utc)

        # Style
        bold = workbook.add_format({'bold': True})
        money_format = workbook.add_format({'num_format': '"Rp." #,##0', 'border': 1})
        money_format_bold = workbook.add_format({'bold': True, 'num_format': '"Rp." #,##0', 'border': 1})
        date_format = workbook.add_format({'num_format': 'd mmmm yyyy', 'border': 1})
        border = workbook.add_format({'border': 1})
        bold_border = workbook.add_format({'bold': True, 'border': 1})
        money_format = workbook.add_format({'num_format': '"Rp." #,##0', 'border': 1})
        money_format_bold = workbook.add_format({'bold': True, 'num_format': '"Rp." #,##0', 'border': 1})
        date_format = workbook.add_format({'num_format': 'd mmmm yyyy', 'border': 1})

        # Variable
        bulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ]

        bulan_s = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Agu',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        ]
        
        sheet = workbook.add_worksheet("Piutang Bulanan {} {}".format(bulan[month-1], year))
        # Column Width Setup
        sheet.set_column(0, 0, 35) # A
        sheet.set_column(1, 1, 15) # B
        sheet.set_column(2, 2, 32) # C
        sheet.set_column(3, 3, 20) # D

        col = 0
        row = 0

        # Header
        sheet.write(row, col, "Piutang Bulanan {} {}".format(bulan[month-1], year), bold)
        row = row + 2
        sheet.write(row, col, "Nama", bold_border)
        col = col + 1
        sheet.write(row, col, "Kelas", bold_border)
        col = col + 1
        sheet.write(row, col, "Uraian Biaya", bold_border)
        col = col + 1
        sheet.write(row, col, "Jumlah", bold_border)
        
        row = row + 1
        start_row = row
        for kewajiban in self.env['efarina.siswa.kewajiban'].search([('year', '=', year), ('month', '=', month), ('sisa_harus_bayar', '>', 0)]).sorted(lambda x: x.siswa_id, False):
            col = 0
            sheet.write(row, col, kewajiban.siswa_id.name, border)
            col = col + 1
            sheet.write(row, col, kewajiban.siswa_id.kelas_id.name, border)
            col = col + 1
            sheet.write(row, col, kewajiban.name, border)
            col = col + 1
            sheet.write(row, col, kewajiban.sisa_harus_bayar, money_format)

            row = row + 1

        col = 0
        sheet.merge_range('A{}:C{}'.format(row+1, row+1), "Total", bold_border)
        col = col + 3
        sheet.write(row, col, "=SUM(D{}:D{})".format(start_row+1, row), money_format_bold)