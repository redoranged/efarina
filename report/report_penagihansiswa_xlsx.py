# -*- coding: utf-8 -*-

import logging, datetime, xlsxwriter, calendar, pytz
from dateutil.relativedelta import relativedelta
import io
from urllib.request import urlopen
import base64

from odoo import models
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class LaporanPenagihanSiswa(models.AbstractModel):
    _name = 'report.efarina.penagihansiswa_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, order):
        biaya_ids = data['context'].get('biaya_ids')
        start_year = int(data['context'].get('start_year'))
        start_month = int(data['context'].get('start_month'))
        end_year = int(data['context'].get('end_year'))
        end_month = int(data['context'].get('end_month'))
        semester = data['context'].get('semester')
        perihal = data['context'].get('perihal') or "Tagihan Administrasi"
        keterangan_bank = data['context'].get('keterangan_bank') or "No Rekening BRI : 0113.01.000.606.30.0 An Yayasan Efarina (wa 082272556685)"
        siswa_ids = data['context'].get('siswa_ids')

        total_days_of_month = calendar.monthrange(end_year, end_month)[1]
        date_start = datetime.date(start_year, start_month, 1)
        date_end = datetime.date(end_year, end_month, total_days_of_month)

        user_tz = pytz.timezone(data['context'].get('tz') or self.env.user.tz or pytz.utc)

        today = datetime.datetime.now()

        # Style
        text = workbook.add_format({
            'font_name': 'Times New Roman',
            'font_size': 14
        })
        text_bold = workbook.add_format({
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True
        })
        text_bold_center = workbook.add_format({
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True,
            'align': 'center'
        })
        text_bold_underline = workbook.add_format({
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True,
            'underline': 1
        })
        text_bold_underline_center = workbook.add_format({
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True,
            'underline': 1,
            'align': 'center'
        })
        table = workbook.add_format({
            'font_size': 10,
            'border': 1
        })
        table_no = workbook.add_format({
            'font_size': 10,
            'border': 1,
            'left': 2
        })
        table_header = workbook.add_format({
            'font_size': 10,
            'border': 1,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        table_header_top = workbook.add_format({
            'font_size': 10,
            'border': 1,
            'top': 2,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        table_header_no = workbook.add_format({
            'font_size': 10,
            'border': 1,
            'top': 2,
            'left': 2,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        table_header_total = workbook.add_format({
            'font_size': 10,
            'border': 1,
            'top': 2,
            'right': 2,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        table_header_14 = workbook.add_format({
            'font_size': 14,
            'border': 1,
            'bottom': 2,
            'left': 2,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        deskripsi = workbook.add_format({
            'font_size': 11,
            'bold': True,
            'italic': True
        })

        bold = workbook.add_format({'bold': True})
        money_format = workbook.add_format({'font_size': 10, 'num_format': '#,##0', 'border': 1})
        money_format_bold = workbook.add_format({'font_size': 10, 'bold': True, 'num_format': '#,##0', 'border': 1})
        money_format_bold_right = workbook.add_format({'font_size': 10, 'bold': True, 'num_format': '#,##0', 'border': 1, 'right': 2})
        money_format_bold_total = workbook.add_format({'font_size': 10, 'bold': True, 'num_format': '#,##0', 'border': 1, 'right': 2, 'bottom': 2})
        date_format = workbook.add_format({'num_format': 'd mmmm yyyy', 'border': 1})
        border = workbook.add_format({'border': 1})
        bold_border = workbook.add_format({'bold': True, 'border': 1})

        # Variable
        bulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ]
        bulan_s = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Agu',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        ]

        bulan_semester = []

        for i in range(1,7):
            nama_bulan = "{}".format(date_start.strftime('%b-%y'))
            bulan_semester.append(nama_bulan)
            date_start += relativedelta(months=1)
            _logger.info(nama_bulan)

        siswa = self.env['efarina.siswa'].browse(siswa_ids)
        for siswa_id in siswa:
            # Resume
            sheet = workbook.add_worksheet("Penagihan {}".format(siswa_id.name))

            # Header
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            if siswa_id.pendidikan == 'SMA':
                header1 = io.BytesIO(urlopen(base_url+"/efarina/static/src/img/kop-sma.png").read())
                sheet.insert_image('B1', 'Header Report SMA.png', {'image_data': header1})

            if siswa_id.pendidikan == 'SMK':
                header1 = io.BytesIO(urlopen(base_url+"/efarina/static/src/img/kop-smk.png").read())
                sheet.insert_image('B1', 'Header Report SMK.png', {'image_data': header1})

            # Kepada
            sheet.write(6, 2, 'Kepada Yth', text_bold)
            sheet.write(6, 4, ':', text)
            sheet.write(6, 5, 'Bapak/Ibu Orang Tua dari', text)
            sheet.write(7, 3, 'Nama', text)
            sheet.write(7, 4, ':', text)
            sheet.write(7, 5, siswa_id.name, text_bold)
            sheet.write(8, 3, 'Kelas', text)
            sheet.write(8, 4, ':', text)
            sheet.write(8, 5, siswa_id.kelas_id.name, text_bold)

            sheet.write(10, 2, 'Perihal', text_bold)
            sheet.write(10, 4, ':', text)
            sheet.write(10, 5, perihal, text_bold_underline)

            # Dengan Hormat
            sheet.write(13, 3, 'Dengan Hormat,', text)
            sheet.write(14, 3, 'Berikut ini kami sampaikan tunggakan siswa tersebut diatas:', text)

            # Table
            sheet.set_row(15, 15)
            sheet.merge_range('C16:C17', 'NO', table_header_no)
            sheet.merge_range('D16:F17', 'RINCIAN BIAYA', table_header_top)
            sheet.merge_range('G16:M16', '', table_header_top)
            sheet.write(16, 6, 'Semester {}'.format(semester), table_header)
            row = 16
            col = 7
            sheet.set_row(row, 15)
            for semester_bulan in bulan_semester:
                sheet.write(row, col, semester_bulan, table_header)
                col += 1
            sheet.merge_range('N16:N17', 'TOTAL', table_header_total)

            biaya_ids = []

            for kewajiban_id in siswa_id.kewajiban_ids:
                if kewajiban_id.biaya_id.id not in biaya_ids:
                    biaya_ids.append(kewajiban_id.biaya_id.id)

            biaya_ids = self.env['efarina.biaya'].browse(biaya_ids)

            row = 17
            col = 2
            no = 1
            first_row = row
            for biaya_id in biaya_ids:
                sheet.set_row(row, 15)
                sheet.write(row, col, no, table_no)
                sheet.merge_range('D{}:F{}'.format(row+1, row+1), biaya_id.name, table)
                if biaya_id.type != 'monthly':
                    domain = [
                        ('siswa_id', '=', siswa_id.id),
                        ('biaya_id', '=', biaya_id.id)
                    ]
                    jumlah = sum(self.env['efarina.siswa.kewajiban'].search(domain).mapped('sisa_harus_bayar'))
                    sheet.write(row, col+4, jumlah, money_format)
                    col_bulan = 7
                    for i in range(1,7):
                        sheet.write(row, col_bulan, 0, money_format)
                        col_bulan += 1
                else:
                    sheet.write(row, col+4, 0, money_format)
                    date_start = datetime.date(start_year, start_month, 1)
                    col_bulan = 7
                    for i in range(1,7):
                        domain = [
                            ('siswa_id', '=', siswa_id.id),
                            ('biaya_id', '=', biaya_id.id),
                            ('month', '=', str(date_start.month)),
                            ('year', '=', str(date_start.year)),
                        ]
                        jumlah = sum(self.env['efarina.siswa.kewajiban'].search(domain).mapped('sisa_harus_bayar'))
                        sheet.write(row, col_bulan, jumlah, money_format)
                        col_bulan += 1
                        date_start += relativedelta(months=1)

                sheet.write(row, 13, '=SUM(G{}:M{})'.format(row+1, row+1), money_format_bold_right)
                no += 1
                row += 1

            sheet.merge_range('C{}:M{}'.format(row+1, row+1), 'Total', table_header_14)
            sheet.write(row, 13, '=SUM(N{}:N{})'.format(first_row+1, row), money_format_bold_total)

            row += 1
            sheet.set_row(row, 9.75)
            row += 1

            # Pesan
            sheet.write_rich_string(row, 3, text, 'Kami sangat mengharapkan Bapak/Ibu dapat menyelesaikan ', text_bold, 'seluruh tagihan administrasi')
            row += 1
            sheet.write(row, 2, 'tersebut, agar tidak menghambat operasional sekolah dalam mendukung kegiatan putra/putri', text)
            row += 1
            sheet.write(row, 2, 'yang dimaksud.', text)
            row += 1
            sheet.write(row, 3, 'Atas perhatian dan kerja sama Bapak/Ibu kami ucapkan terima kasih.', text)

            row += 2
            # Tanda Tangan
            sheet.merge_range('J{}:O{}'.format(row+1, row+1), 'Saribudolok, {}'.format(pytz.utc.localize(today).astimezone(user_tz).strftime("%d %B %Y")), text_bold_center)
            row += 1
            sheet.merge_range('J{}:O{}'.format(row+1, row+1), 'Hormat Kami', text_bold_center)
            row += 4
            sheet.merge_range('J{}:O{}'.format(row+1, row+1), 'Benny Sofian P Purba, SE', text_bold_underline_center)
            row += 1
            sheet.merge_range('J{}:O{}'.format(row+1, row+1), 'Keuangan', text_bold_center)

            row += 4
            sheet.write(row, 2, '*', deskripsi)
            sheet.write(row, 3, 'Pembayaran Uang Sekolah dan asrama dibayarkan selambat lambatnya tanggal 10 setiap bulannya', deskripsi)
            row += 1
            sheet.write(row, 2, '*', deskripsi)
            sheet.write(row, 3, keterangan_bank, deskripsi)

            # Page Setup
            sheet.set_paper(9)
            sheet.print_area('B1:O{}'.format(row+1))
            sheet.center_horizontally()
            sheet.set_margins(top=0.51, bottom=0.75, left=0.12, right=0.12)
            sheet.set_header('', {'margin': 0.315})
            sheet.set_footer('', {'margin': 0.315})
            sheet.set_print_scale(85)

            # Column Width Setup
            sheet.set_column(0, 0, 11.43) # A
            sheet.set_column(1, 1, 1.29) # B
            sheet.set_column(2, 2, 3.14) # C
            sheet.set_column(3, 3, 11.14) # D
            sheet.set_column(4, 4, 1.43) # E
            sheet.set_column(5, 5, 8.57) # F
            sheet.set_column(6, 6, 10) # G
            sheet.set_column(7, 12, 9) # H - M
            sheet.set_column(13, 13, 11) # N
            sheet.set_column(14, 14, 1.29) # O

            # Row Height Setup
            sheet.set_default_row(18.75)
            row_setup = [
                [0, 15],
                [2, 16.5],
                [3, 33.75],
                [4, 33.75],
                [5, 7.5],
                [6, 15.75],
                [7, 15.75],
                [8, 15.75],
                [9, 8.25],
                [14, 19.5],
            ]
            for row_s in row_setup:
                sheet.set_row(row_s[0], row_s[1])