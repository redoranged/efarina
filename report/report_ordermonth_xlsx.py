# -*- coding: utf-8 -*-

from odoo import models, _
from odoo.exceptions import UserError

import datetime, calendar, pytz

import logging, xlsxwriter

_logger = logging.getLogger(__name__)

class LaporanTransaksiBulanan(models.AbstractModel):
    _name = 'report.efarina.ordermonth_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, order):
        start_year = int(data['context'].get('start_year'))
        start_month = int(data['context'].get('start_month'))
        end_year = int(data['context'].get('end_year'))
        end_month = int(data['context'].get('end_month'))

        # date_start = datetime.date(start_year, start_month, 1)
        # date_end = datetime.date(end_year, end_month, 1)

        # year = int(data['context'].get('year'))
        # month = int(data['context'].get('month'))

        total_days_of_month = calendar.monthrange(end_year, end_month)[1]

        # start_datetime = datetime.datetime.strptime("{}/{}/{}".format(start_year,start_month,1), "%Y/%m/%d")
        # end_datetime = datetime.datetime.strptime("{}/{}/{}".format(end_year,end_month,total_days_of_month), "%Y/%m/%d")
        start_datetime = datetime.datetime.strptime("{}-{}-{} 00:00:00".format(start_year,start_month,1), "%Y-%m-%d %H:%M:%S")
        end_datetime = datetime.datetime.strptime("{}-{}-{} 23:59:59".format(end_year,end_month,total_days_of_month), "%Y-%m-%d %H:%M:%S")

        user_tz = pytz.timezone(data['context'].get('tz') or self.env.user.tz or pytz.utc)
        start_utc_datetime = user_tz.localize(start_datetime).astimezone(pytz.utc)
        end_utc_datetime = user_tz.localize(end_datetime).astimezone(pytz.utc)

        # for order in self.env['efarina.order'].search([('type', '=', 'in'), ('state', 'in', ['confirmed', 'done']), ('date_order', '>', start_utc_datetime.replace(tzinfo=None)), ('date_order', '<', end_utc_datetime.replace(tzinfo=None))], order='date_order'):
        #     delta = datetime.timedelta(days=1)
        #     date_order_tomorow = order.date_order + delta
        #     tanggal = pytz.utc.localize(order.date_order).astimezone(user_tz).strftime("%Y-%m-%d")
        #     tanggal_tomorow = pytz.utc.localize(date_order_tomorow).astimezone(user_tz).strftime("%Y-%m-%d")

        #     _logger.info("****************************************")
        #     # _logger.info(data)
        #     _logger.info(order.name)
        #     _logger.info(order.nama)
        #     _logger.info(data['context'].get('tz'))
        #     _logger.info(order.date_order)
        #     _logger.info(start_datetime)
        #     _logger.info(end_datetime)
        #     _logger.info(start_utc_datetime)
        #     _logger.info(start_utc_datetime + datetime.timedelta(days=1))
        #     _logger.info(end_utc_datetime)
        #     _logger.info(tanggal)
        #     _logger.info(date_order_tomorow)
        #     _logger.info(tanggal_tomorow)
        #     _logger.info("****************************************")
        # raise UserError(_(start_datetime))

        # Style
        bold = workbook.add_format({'bold': True})
        money_format = workbook.add_format({'num_format': '"Rp." #,##0', 'border': 1})
        money_format_bold = workbook.add_format({'bold': True, 'num_format': '"Rp." #,##0', 'border': 1})
        date_format = workbook.add_format({'num_format': 'd mmmm yyyy', 'border': 1, 'valign': 'top'})
        bold_border = workbook.add_format({
            'bold': True,
            'border': 1,
        })
        bold_center_border = workbook.add_format({
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1
        })
        border = workbook.add_format({
            'border': 1
        })
        
        # Variable
        bulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ]

        # Kas Masuk
        sheet_kas_masuk = workbook.add_worksheet('Kas Masuk')
        # Column Width Setup
        sheet_kas_masuk.set_column(0, 0, 12) # A
        sheet_kas_masuk.set_column(1, 2, 35) # B - C
        sheet_kas_masuk.set_column(3, 3, 30) # D
        sheet_kas_masuk.set_column(4, 4, 15) # E

        # Header
        sheet_kas_masuk.write(0, 1, "Pemasukan Bulan {} {} - {} {}".format(bulan[start_month-1], start_year, bulan[end_month-1], end_year), bold)

        # COLUMN
        column_name = [
            "TANGGAL",
            "NAMA",
            "KETERANGAN",
        ]
        col_end = 1
        # for jenis_pendapatan in self.env['efarina.jenispendapatan'].search([]):
        #     column_name.append(str.upper(jenis_pendapatan.name))
        #     col_end = col_end + 1
        column_name.append("JENIS PENDAPATAN")
        column_name.append("JUMLAH")

        col = 0
        for header in column_name:
            sheet_kas_masuk.write(1, col, column_name[col], bold_border)
            col = col+1

        row = 2
        col = 0

        total = 0
        tanggal_temp = ""
        start_row = row
        start_row_date = row
        # for obj in self.env['efarina.order'].search([('type', '=', 'in'), ('state', 'in', ['confirmed', 'done']), ('date_order', '>', start_datetime), ('date_order', '<', end_datetime)], order='date_order'):
        for obj in self.env['efarina.order'].search([('type', '=', 'in'), ('state', 'in', ['confirmed', 'done']), ('date_order', '>', start_utc_datetime.replace(tzinfo=None)), ('date_order', '<', end_utc_datetime.replace(tzinfo=None))], order='date_order'):
            col = 0
            tanggal_baru = True
            delta = datetime.timedelta(days=1)
            date_order_yesterday = obj.date_order - delta
            date_order_tomorow = obj.date_order + delta
            tanggal_yesterday = pytz.utc.localize(date_order_yesterday).astimezone(user_tz).strftime("%Y-%m-%d")
            tanggal = pytz.utc.localize(obj.date_order).astimezone(user_tz).strftime("%Y-%m-%d")
            tanggal_tomorow = pytz.utc.localize(date_order_tomorow).astimezone(user_tz).strftime("%Y-%m-%d")
            nama = obj.nama
            keterangan = "{} {}".format(obj.name, '| {}'.format(obj.note) if obj.note else '')

            if tanggal_temp != tanggal:
                # Ganti tanggal muncul Total
                if tanggal_temp != "":
                    if row == start_row_date:
                        sheet_kas_masuk.write(start_row_date - 1, col, tanggal_yesterday, date_format)
                    else:
                        sheet_kas_masuk.merge_range('A{}:A{}'.format(start_row_date, row), tanggal_yesterday, date_format)
                    for header in column_name:
                        if header == "KETERANGAN":
                            sheet_kas_masuk.merge_range('A{}:D{}'.format(row+1, row+1), "TOTAL", bold_center_border)
                        if header == "JUMLAH":
                            sheet_kas_masuk.write(row, col, '=SUM({}:{})'.format("{}{}".format(xlsxwriter.utility.xl_col_to_name(col), start_row+1), "{}{}".format(xlsxwriter.utility.xl_col_to_name(col), row)), money_format_bold)
                        col = col + 1
                    row = row + 1
                    total = 0
                tanggal_temp = tanggal
                start_row = row
            else:
                tanggal_baru = False
            col = 0
            
            for header in column_name:
                if header == "TANGGAL":
                    if tanggal_baru:
                        start_row_date = row + 1
                        # sheet_kas_masuk.write(row, col, tanggal, date_format)
                if header == "NAMA":
                    sheet_kas_masuk.write(row, col, nama, border)
                if header == "KETERANGAN":
                    sheet_kas_masuk.write(row, col, keterangan, border)
                if header == "JENIS PENDAPATAN":
                    sheet_kas_masuk.write(row, col, obj.jenispendapatan_id.name if obj.jenispendapatan_id else "Pendapatan Lainnya", border)
                if obj.jenispendapatan_id:
                    if header == str.upper(obj.jenispendapatan_id.name):
                        sheet_kas_masuk.write(row, col, obj.jumlah, money_format)
                if header == "JUMLAH":
                    sheet_kas_masuk.write(row, col, obj.jumlah, money_format)
                col = col + 1

            row = row + 1

            if row == start_row_date:
                sheet_kas_masuk.write(start_row_date - 1, col, tanggal, date_format)
            else:
                sheet_kas_masuk.merge_range('A{}:A{}'.format(start_row_date, row), tanggal, date_format)

        # Total Terakhir
        col = 0
        for header in column_name:
            if header == "KETERANGAN":
                sheet_kas_masuk.merge_range('A{}:D{}'.format(row+1, row+1), "TOTAL", bold_center_border)
            if header == "JUMLAH":
                sheet_kas_masuk.write(row, col, '=SUM({}:{})'.format("{}{}".format(xlsxwriter.utility.xl_col_to_name(col), start_row+1), "{}{}".format(xlsxwriter.utility.xl_col_to_name(col), row)), money_format_bold)
            col = col + 1

        # Kas Keluar
        sheet_kas_keluar = workbook.add_worksheet('Kas Keluar')
        # Column Width Setup
        sheet_kas_keluar.set_column(0, 0, 12) # A
        sheet_kas_keluar.set_column(1, 2, 35) # B - C
        sheet_kas_keluar.set_column(3, 3, 30) # D
        sheet_kas_keluar.set_column(4, 6, 15) # E - G

        # Header
        sheet_kas_keluar.write(0, 1, "Pengeluaran Bulan {} {} - {} {}".format(bulan[start_month-1], start_year, bulan[end_month-1], end_year), bold)

        # COLUMN
        column_name = [
            "TANGGAL",
            "NAMA",
            "KETERANGAN",
            "JENIS PENGELUARAN",
            "OPERASIONAL",
            "NON OPERASIONAL",
            "JUMLAH",
        ]
        col = 0
        for header in column_name:
            sheet_kas_keluar.write(1, col, column_name[col], bold_border)
            col = col+1

        row = 2
        col = 0

        tanggal_temp = ""
        start_row = row
        start_row_date = row
        for obj in self.env['efarina.order'].search([('type', '=', 'out'), ('state', 'in', ['confirmed', 'done']), ('date_order', '>', start_utc_datetime.replace(tzinfo=None)), ('date_order', '<', end_utc_datetime.replace(tzinfo=None))], order='date_order'):
            col = 0
            tanggal_baru = True
            delta = datetime.timedelta(days=1)
            date_order_yesterday = obj.date_order - delta
            date_order_tomorow = obj.date_order + delta
            tanggal_yesterday = pytz.utc.localize(date_order_yesterday).astimezone(user_tz).strftime("%Y-%m-%d")
            tanggal = pytz.utc.localize(obj.date_order).astimezone(user_tz).strftime("%Y-%m-%d")
            tanggal_tomorow = pytz.utc.localize(date_order_tomorow).astimezone(user_tz).strftime("%Y-%m-%d")
            nama = obj.nama
            keterangan = "{} {}".format(obj.name, '| {}'.format(obj.note) if obj.note else '')
            out_type = "Operasional" if obj.out_type == 'operasional' else "Non Operasional"
            jenis_pengeluaran = obj.jenispengeluaran_id.name if obj.jenispengeluaran_id else ""

            if tanggal_temp != tanggal:
                # Ganti tanggal muncul Total
                if tanggal_temp != "":
                    if row == start_row_date:
                        sheet_kas_keluar.write(start_row_date - 1, col, tanggal_yesterday, date_format)
                    else:
                        sheet_kas_keluar.merge_range('A{}:A{}'.format(start_row_date, row), tanggal_yesterday, date_format)
                    for header in column_name:
                        if header == "KETERANGAN":
                            sheet_kas_keluar.merge_range('A{}:D{}'.format(row+1, row+1), "TOTAL", bold_center_border)
                        if header == "OPERASIONAL":
                            sheet_kas_keluar.write(row, col, "=SUM(E{}:E{})".format(start_row+1, row), money_format_bold)
                        if header == "NON OPERASIONAL":
                            sheet_kas_keluar.write(row, col, "=SUM(F{}:F{})".format(start_row+1, row), money_format_bold)
                        if header == "JUMLAH":
                            sheet_kas_keluar.write(row, col, "=SUM(E{}:F{})".format(row+1, row+1), money_format_bold)
                        col = col + 1
                    row = row + 1
                    start_row = row
                tanggal_temp = tanggal
            else:
                tanggal_baru = False
            col = 0
            
            for header in column_name:
                if header == "TANGGAL":
                    if tanggal_baru:
                        start_row_date = row + 1
                        # sheet_kas_keluar.write(row, col, tanggal, date_format)
                if header == "NAMA":
                    sheet_kas_keluar.write(row, col, nama, border)
                if header == "JENIS PENGELUARAN":
                    sheet_kas_keluar.write(row, col, jenis_pengeluaran, border)
                if header == "KETERANGAN":
                    sheet_kas_keluar.write(row, col, keterangan, border)
                if header == "OPERASIONAL":
                    if obj.out_type == 'operasional':
                        sheet_kas_keluar.write(row, col, obj.jumlah, money_format)
                        sheet_kas_keluar.write(row, col+1, '', border)
                        sheet_kas_keluar.write(row, col+2, '', border)
                if header == "NON OPERASIONAL":
                    if obj.out_type == 'non':
                        sheet_kas_keluar.write(row, col, obj.jumlah, money_format)
                        sheet_kas_keluar.write(row, col-1, '', border)
                        sheet_kas_keluar.write(row, col+1, '', border)
                col = col + 1

            row = row + 1

            if row == start_row_date:
                sheet_kas_keluar.write(start_row_date - 1, col, tanggal, date_format)
            else:
                sheet_kas_keluar.merge_range('A{}:A{}'.format(start_row_date, row), tanggal, date_format)

        # Total Terakhir
        col = 0
        for header in column_name:
            if header == "KETERANGAN":
                sheet_kas_keluar.merge_range('A{}:D{}'.format(row+1, row+1), "TOTAL", bold_center_border)
            if header == "OPERASIONAL":
                sheet_kas_keluar.write(row, col, "=SUM(E{}:E{})".format(start_row+1, row), money_format_bold)
            if header == "NON OPERASIONAL":
                sheet_kas_keluar.write(row, col, "=SUM(F{}:F{})".format(start_row+1, row), money_format_bold)
            if header == "JUMLAH":
                sheet_kas_keluar.write(row, col, "=SUM(E{}:F{})".format(row+1, row+1), money_format_bold)
            col = col + 1

        # Arus Kas
        start_datetime = datetime.datetime.strptime("{}/{}/{}".format(start_year,start_month,1), "%Y/%m/%d")
        end_datetime = datetime.datetime.strptime("{}/{}/{} 23:59:59".format(end_year,end_month,total_days_of_month), "%Y/%m/%d %H:%M:%S")
        
        sheet_arus_kas = workbook.add_worksheet('Arus Kas')
        # Column Width Setup
        sheet_arus_kas.set_column(0, 0, 4) # A
        sheet_arus_kas.set_column(1, 1, 17) # B

        row = 0
        col = 0
        # Header
        sheet_arus_kas.merge_range("A1:A2", "No", bold_center_border)
        col = col + 1
        sheet_arus_kas.merge_range("B1:B2", "Tanggal Transaksi", bold_center_border)
        col = col + 1

        for jenis_pendapatan in self.env['efarina.jenispendapatan'].search([]):
            if jenis_pendapatan.detail_aset:
                first_col = col
                for aset in self.env['efarina.aset'].search([]):
                    sheet_arus_kas.set_column(col, col, 15)
                    sheet_arus_kas.write(row+1, col, aset.name, bold_border)
                    col = col + 1
                sheet_arus_kas.merge_range("{}{}:{}{}".format(xlsxwriter.utility.xl_col_to_name(first_col), row+1, xlsxwriter.utility.xl_col_to_name(col-1), row+1), jenis_pendapatan.name, bold_center_border)
            else:
                sheet_arus_kas.write(row, col, jenis_pendapatan.name, bold_border)
                sheet_arus_kas.set_column(col, col, 15)
                col = col + 1

        # sheet_arus_kas.write(row, col, "Pendapatan Lainnya", bold_border)
        # col = col + 1
        sheet_arus_kas.write(row, col, "Pengeluaran", bold_border)
        col = col + 1
        sheet_arus_kas.write(row, col, "Saldo Akhir", bold_border)
        for aset in self.env['efarina.aset'].search([]):
            sheet_arus_kas.write(row+1, col, aset.name, bold_border)
            col = col + 1
        row = row + 2
        
        col = 0
        delta = datetime.timedelta(days=1)
        no = 1
        
        _logger.info(start_datetime)
        _logger.info(end_datetime
        )
        while start_datetime <= end_datetime:
            # No
            sheet_arus_kas.write(row, col, no, border)
            col = col + 1
            # Tanggal
            sheet_arus_kas.write(row, col, start_datetime.strftime("%Y-%m-%d"), date_format)
            col = col + 1

            start_utc_datetime = user_tz.localize(start_datetime).astimezone(pytz.utc)
            end_utc_datetime = user_tz.localize(end_datetime).astimezone(pytz.utc)

            for jenis_pendapatan in self.env['efarina.jenispendapatan'].search([]):
                if jenis_pendapatan.detail_aset:
                    for aset in self.env['efarina.aset'].search([]):
                        sheet_arus_kas.write(row, col, sum(jenis_pendapatan.order_ids.filtered(lambda x: x.type == 'in' and x.aset_id.id == aset.id and x.state in ['confirmed', 'done'] and x.date_order > start_utc_datetime.replace(tzinfo=None) and x.date_order < start_utc_datetime.replace(tzinfo=None) + datetime.timedelta(days=1)).mapped('jumlah')), money_format)
                        col = col + 1
                else:
                    sheet_arus_kas.write(row, col, sum(jenis_pendapatan.order_ids.filtered(lambda x: x.type == 'in' and x.state in ['confirmed', 'done'] and x.date_order > start_utc_datetime.replace(tzinfo=None) and x.date_order < start_utc_datetime.replace(tzinfo=None) + datetime.timedelta(days=1)).mapped('jumlah')), money_format)
                    col = col + 1
            # Pendapatan Lainnya
            # sheet_arus_kas.write(row, col, sum(order.jumlah for order in self.env['efarina.order'].search([('type', '=', 'in'), ('jenispendapatan_id', '=', False), ('state', 'in', ['confirmed', 'done']), ('date_order', '>', start_utc_datetime.replace(tzinfo=None)), ('date_order', '<', start_utc_datetime.replace(tzinfo=None) + datetime.timedelta(days=1))], order='date_order')), 
            # money_format)
            # col = col + 1
            # Pengeluaran
            sheet_arus_kas.write(row, col, sum(order.jumlah for order in self.env['efarina.order'].search([('type', '=', 'out'), ('state', 'in', ['confirmed', 'done']), ('date_order', '>', start_utc_datetime.replace(tzinfo=None)), ('date_order', '<', start_utc_datetime.replace(tzinfo=None) + datetime.timedelta(days=1))])), money_format)
            col = col + 1
            for aset in self.env['efarina.aset'].search([]):
                total_income_before = sum(aset.order_ids.filtered(lambda x: x.type == 'in' and x.state in ['confirmed', 'done'] and x.date_order < start_utc_datetime.replace(tzinfo=None) + datetime.timedelta(days=1)).mapped('jumlah'))
                total_outcome_before = sum(aset.order_ids.filtered(lambda x: x.type == 'out' and x.state in ['confirmed', 'done'] and x.date_order < start_utc_datetime.replace(tzinfo=None) + datetime.timedelta(days=1)).mapped('jumlah'))
                sheet_arus_kas.write(row, col, total_income_before - total_outcome_before, money_format_bold)
                col = col + 1

            col = 0
            row = row + 1
            no = no + 1

            start_datetime += delta

        # No
        # sheet_arus_kas.write(row, col, no)
        col = col + 1
        # Tanggal
        # sheet_arus_kas.write(row, col, start_datetime.strftime("%Y-%m-%d"), date_format)
        col = col + 1

        for jenis_pendapatan in self.env['efarina.jenispendapatan'].search([]):
            if jenis_pendapatan.detail_aset:
                for aset in self.env['efarina.aset'].search([]):
                    sheet_arus_kas.write(row, col, '=SUM({}{}:{}{})'.format(xlsxwriter.utility.xl_col_to_name(col), 3, xlsxwriter.utility.xl_col_to_name(col), total_days_of_month + 2), money_format)
                    col = col + 1
            else:
                sheet_arus_kas.write(row, col, '=SUM({}{}:{}{})'.format(xlsxwriter.utility.xl_col_to_name(col), 3, xlsxwriter.utility.xl_col_to_name(col), total_days_of_month + 2), money_format)
                col = col + 1
        # sheet_arus_kas.write(row, col, '=SUM({}{}:{}{})'.format(xlsxwriter.utility.xl_col_to_name(col), 3, xlsxwriter.utility.xl_col_to_name(col), total_days_of_month + 2), money_format)
        # col = col + 1
        sheet_arus_kas.write(row, col, '=SUM({}{}:{}{})'.format(xlsxwriter.utility.xl_col_to_name(col), 3, xlsxwriter.utility.xl_col_to_name(col), total_days_of_month + 2), money_format)
        col = col + 1
        for aset in self.env['efarina.aset'].search([]):
            sheet_arus_kas.write(row, col, '={}{}'.format(xlsxwriter.utility.xl_col_to_name(col), row), money_format)
            col = col + 1