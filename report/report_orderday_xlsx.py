# -*- coding: utf-8 -*-

import logging, datetime, xlsxwriter 
import pytz

from odoo import models
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class LaporanTransaksiHarian(models.AbstractModel):
    _name = 'report.efarina.orderday_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, order):
        start_datetime = datetime.datetime.strptime("{} 00:00:00".format(data['context'].get('date_start')), "%Y-%m-%d %H:%M:%S")
        end_datetime = datetime.datetime.strptime("{} 23:59:59".format(data['context'].get('date_end')), "%Y-%m-%d %H:%M:%S")

        user_tz = pytz.timezone(data['context'].get('tz') or self.env.user.tz or pytz.utc)
        start_utc_datetime = user_tz.localize(start_datetime).astimezone(pytz.utc)
        end_utc_datetime = user_tz.localize(end_datetime).astimezone(pytz.utc)

        # for order in self.env['efarina.order'].search([]):
        #     _logger.info("****************************************")
        #     _logger.info(data)
        #     _logger.info(data['context'].get('tz'))
        #     _logger.info(order.date_order)
        #     _logger.info(start_datetime)
        #     _logger.info(end_datetime)
        #     _logger.info(start_utc_datetime)
        #     _logger.info(start_utc_datetime + datetime.timedelta(days=1))
        #     _logger.info(end_utc_datetime)
        #     _logger.info("****************************************")
        # raise UserError(start_datetime)

        # Style
        bold = workbook.add_format({'bold': True})
        money_format = workbook.add_format({'num_format': '"Rp." #,##0', 'border': 1})
        money_format_bold = workbook.add_format({'bold': True, 'num_format': '"Rp." #,##0', 'border': 1})
        date_format = workbook.add_format({'num_format': 'd mmmm yyyy', 'border': 1})
        bold_border = workbook.add_format({
            'bold': True,
            'border': 1,
        })
        bold_center = workbook.add_format({
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        bold_center_border = workbook.add_format({
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1
        })
        border = workbook.add_format({
            'border': 1
        })

        # Variable
        bulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ]

        delta = datetime.timedelta(days=1)

        while start_datetime <= end_datetime:
            sheet = workbook.add_worksheet("{} {} {}".format(start_datetime.day, bulan[start_datetime.month-1], start_datetime.year))
            # Column Width Setup
            sheet.set_column(0, 0, 4) # A
            sheet.set_column(1, 1, 10) # B
            sheet.set_column(2, 4, 30) # C - E
            sheet.set_column(5, 6, 15) # F - G

            start_utc_datetime = user_tz.localize(start_datetime).astimezone(pytz.utc)
            end_utc_datetime = user_tz.localize(end_datetime).astimezone(pytz.utc)

            # Start Sheet
            # Header
            aset_column = len(self.env['efarina.aset'].search([]))

            sheet.merge_range('B1:{}1'.format(xlsxwriter.utility.xl_col_to_name(6+aset_column)), "Laporan Penerimaan & Pengeluaran Harian", bold_center)
            sheet.merge_range('B2:{}2'.format(xlsxwriter.utility.xl_col_to_name(6+aset_column)), "SMA/SMK Plus Kesehata Efarina", bold_center)

            # Table Header 1
            sheet.merge_range('B5:G6', "Saldo Awal", bold_center_border)
            col = 7
            saldo = {}
            for aset in self.env['efarina.aset'].search([]):
                sheet.set_column(col, col, 15)
                sheet.write(4, col, aset.name, bold_border)
                total_income_before = sum(aset.order_ids.filtered(lambda x: x.type == 'in' and x.state in ['confirmed', 'done'] and x.date_order < start_utc_datetime.replace(tzinfo=None)).mapped('jumlah'))
                total_outcome_before = sum(aset.order_ids.filtered(lambda x: x.type == 'out' and x.state in ['confirmed', 'done'] and x.date_order < start_utc_datetime.replace(tzinfo=None)).mapped('jumlah'))
                # _logger.info(str.upper(aset.name))
                # _logger.info("Income : {}".format(total_income_before))
                # _logger.info("Outcome : {}".format(total_outcome_before))
                sheet.write(5, col, total_income_before - total_outcome_before, money_format_bold)
                saldo[aset.id] = total_income_before - total_outcome_before
                col = col + 1
            last_col = col

            # Table Header 2
            row = 7
            sheet.write(row, 1, "Penerima", border)
            sheet.write(row, 2, "Nama", border)
            sheet.write(row, 3, "Keterangan", border)
            sheet.write(row, 4, "Jenis", border)
            sheet.write(row, 5, "Debit", border)
            sheet.write(row, 6, "Kredit", border)
            col = 7
            for aset in self.env['efarina.aset'].search([]):
                sheet.write(row, col, aset.name, border)
                col = col + 1
            
            row = row + 1
            sheet.merge_range('B9:{}9'.format(xlsxwriter.utility.xl_col_to_name(last_col-1)), "Pendapatan", bold_center_border)
            row = row + 1
            first = True
            for order in self.env['efarina.order'].search([('type', '=', 'in'), ('state', 'in', ['confirmed', 'done']), ('date_order', '>', start_utc_datetime.replace(tzinfo=None)), ('date_order', '<', start_utc_datetime.replace(tzinfo=None) + datetime.timedelta(days=1))]):
                nama = order.nama
                keterangan = "{} {}".format(order.name, '| {}'.format(order.note) if order.note else '')
                sheet.write(row, 1, '', border)
                sheet.write(row, 2, nama, border)
                sheet.write(row, 3, keterangan, border)
                sheet.write(row, 4, order.jenispendapatan_id.name if order.jenispendapatan_id else "Pendapatan Lainnya", border)
                sheet.write(row, 5, order.jumlah, money_format)
                sheet.write(row, 6, '', border)
                col = 7
                for aset in self.env['efarina.aset'].search([]):
                    if order.aset_id.id == aset.id:
                        saldo[aset.id] = saldo[aset.id] + order.jumlah
                        sheet.write(row, col, saldo[aset.id], money_format)
                    else:
                        sheet.write(row, col, '', border)
                    col = col + 1
                first = False

                row = row + 1
            end_row = row
            if end_row == 9:
                row = row + 1
                end_row = row
            sheet.merge_range('B{}:E{}'.format(row+1, row+1), 'Total', border)
            sheet.write(row, 5, '=SUM(F10:F{})'.format(end_row), money_format)
            sheet.write(row, 6, '', border)
            col = 7
            for aset in self.env['efarina.aset'].search([]):
                sheet.write(row, col, '=MAX({}10:{},{}6)'.format(xlsxwriter.utility.xl_col_to_name(col), "{}{}".format(xlsxwriter.utility.xl_col_to_name(col), end_row), xlsxwriter.utility.xl_col_to_name(col)), money_format)
                col = col + 1

            row = row + 2
            sheet.merge_range('B{}:{}{}'.format(row+1, xlsxwriter.utility.xl_col_to_name(last_col-1), row+1), "Pengeluaran", bold_center_border)
            row = row + 1
            first_row = row
            first = True
            for order in self.env['efarina.order'].search([('type', '=', 'out'), ('state', 'in', ['confirmed', 'done']), ('date_order', '>', start_utc_datetime.replace(tzinfo=None)), ('date_order', '<', start_utc_datetime.replace(tzinfo=None) + datetime.timedelta(days=1))]):
                nama = order.nama
                keterangan = "{} {}".format(order.name, '| {}'.format(order.note) if order.note else '')
                # sheet.write(row, 1, "%s")
                sheet.write(row, 1, '', border)
                sheet.write(row, 2, nama, border)
                sheet.write(row, 3, keterangan, border)
                sheet.write(row, 4, order.jenispengeluaran_id.name if order.jenispengeluaran_id else "", border)
                sheet.write(row, 5, '', border)
                sheet.write(row, 6, order.jumlah, money_format)
                col = 7
                for aset in self.env['efarina.aset'].search([]):
                    if order.aset_id.id == aset.id:
                        saldo[aset.id] = saldo[aset.id] - order.jumlah
                        sheet.write(row, col, saldo[aset.id], money_format)
                    else:
                        sheet.write(row, col, '', border)
                    col = col + 1
                first = False

                row = row + 1
            sheet.merge_range('B{}:E{}'.format(row+1, row+1), 'Total', border)
            end_row = row
            sheet.write(row, 5, '', border)
            if row == first_row:
                sheet.write(row, 6, '=SUM(G{}:G{})'.format(first_row, end_row), money_format)
            else:
                sheet.write(row, 6, '=SUM(G{}:G{})'.format(first_row+1, end_row), money_format)
            col = 7
            for aset in self.env['efarina.aset'].search([]):
                if row == first_row:
                    sheet.write(row, col, '=MIN({}{}:{},{}{})'.format(xlsxwriter.utility.xl_col_to_name(col), first_row, "{}{}".format(xlsxwriter.utility.xl_col_to_name(col), end_row), xlsxwriter.utility.xl_col_to_name(col), first_row-2), money_format)
                else:
                    sheet.write(row, col, '=MIN({}{}:{},{}{})'.format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, "{}{}".format(xlsxwriter.utility.xl_col_to_name(col), end_row), xlsxwriter.utility.xl_col_to_name(col), first_row-2), money_format)
                col = col + 1

            row = row + 2
            # Table Footer 1
            col = 7
            for aset in self.env['efarina.aset'].search([]):
                sheet.write(row, col, aset.name, bold_border)
                sheet.write(row+1, col, '={}{}'.format(xlsxwriter.utility.xl_col_to_name(col), end_row+1), money_format_bold)
                col = col + 1
            # End Sheet

            start_datetime += delta