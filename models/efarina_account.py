# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

from odoo.osv import expression

import logging

_logger = logging.getLogger(__name__)

class Account(models.Model):
    _name = 'efarina.account'
    _description = "Chart of Account"
    _parent_name = "parent_id"
    _parent_store = True
    _rec_name = 'code'
    _order = 'code'

    sequence = fields.Integer(required=True, default=10)
    name = fields.Char(string="Account Name", required=True, index=True)
    complete_name = fields.Char(
        'Complete Name', compute='_compute_complete_name',
        store=True)
    def _default_indent_name(self):
        return "\\".ljust(4)

    indent_name = fields.Char("Indent Name", compute="_compute_complete_name", store=True, default=_default_indent_name)
    parent_id = fields.Many2one('efarina.account', 'Parent Account', index=True, ondelete='cascade')
    parent_path = fields.Char(index=True)
    child_id = fields.One2many('efarina.account', 'parent_id', 'Child Accounts')

    @api.depends('name', 'parent_id.complete_name', 'indent_name', 'parent_id.indent_name')
    def _compute_complete_name(self):
        for account in self:
            indent = "\\".ljust(4)
            if account.parent_id:
                account.complete_name = '{}{} {}'.format(account.parent_id.indent_name, account.code, account.name)
                account.indent_name = '{}{}'.format(account.parent_id.indent_name, indent)
            else:
                account.complete_name = "%s %s" % (account.code, account.name)
                account.indent_name = indent

    code = fields.Char(size=64, required=True, index=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True,
        default=lambda self: self.env.company)

    _sql_constraints = [
        ('code_company_uniq', 'unique (code,company_id)', 'The code of the account must be unique per company !')
    ]

    @api.model
    def default_get(self, default_fields):
        """If we're creating a new account through a many2one, there are chances that we typed the account code
        instead of its name. In that case, switch both fields values.
        """
        if 'name' not in default_fields and 'code' not in default_fields:
            return super().default_get(default_fields)
        default_name = self._context.get('default_name')
        default_code = self._context.get('default_code')
        if default_name and not default_code:
            try:
                default_code = int(default_name)
            except ValueError:
                pass
            if default_code:
                default_name = False
        contextual_self = self.with_context(default_name=default_name, default_code=default_code)
        return super(Account, contextual_self).default_get(default_fields)

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('code', '=ilike', name.split(' ')[0] + '%'), ('name', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        return self._search(expression.AND([domain, args]), limit=limit, access_rights_uid=name_get_uid)

    def name_get(self):
        result = []
        for account in self:
            name = account.code + ' ' + account.name
            result.append((account.id, name))
        return result