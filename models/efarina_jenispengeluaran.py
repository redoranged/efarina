# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging

_logger = logging.getLogger(__name__)

class JenisPengeluaran(models.Model):
    _name = 'efarina.jenispengeluaran'

    name = fields.Char('Nama Jenis Pengeluaran', required=True)
    
    account_id = fields.Many2one('efarina.account', 'Akun Pengeluaran')
    def _get_default_currency_id(self):
        return self.env.company.currency_id.id
    
    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)
    
    jumlah = fields.Monetary('Jumlah', required=True, default=0)

    order_ids = fields.One2many('efarina.order', 'jenispengeluaran_id', 'Order')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'The name of the Jenis Pengeluaran must be unique !')
    ]

    def name_get(self):
        result = []
        for jenispengeluaran in self:
            name = "{} {}".format(jenispengeluaran.name, "({} {})".format(jenispengeluaran.account_id.code, jenispengeluaran.account_id.name) if jenispengeluaran.account_id else '')
            result.append((jenispengeluaran.id, name))
        return result