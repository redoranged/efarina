# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import UserError

import logging

_logger = logging.getLogger(__name__)

class Tingkatan(models.Model):
    _name = 'efarina.tingkatan'

    name = fields.Char('Nama Tingkatan', required=True, index=True)
    siswa_ids = fields.One2many('efarina.siswa', 'tingkatan_id', 'Siswa')
    siswa_count = fields.Integer('# Siswa',
        compute='_compute_siswa_count', compute_sudo=False)
    tingkat = fields.Selection([
        ('x', 'X'),
        ('xi', 'XI'),
        ('xii', 'XII'),
        ('alumni', 'Alumni')
    ], string="Tingkat", required=True)
    tahun_ajaran = fields.Char()
    active = fields.Boolean(default=True)
    def _compute_siswa_count(self):
        for tingkatan in self:
            tingkatan.siswa_count = self.env['efarina.siswa'].search_count([('tingkatan_id', '=', tingkatan.id)])

    kelas_ids = fields.One2many('efarina.kelas', 'tingkatan_id', 'Kelas')
    kelas_count = fields.Integer('# Kelas',
        compute='_compute_kelas_count', compute_sudo=False)

    def toggle_active(self):
        self.active = not self.active
        return True
        
    @api.depends('kelas_ids')
    def _compute_kelas_count(self):
        for tingkatan in self:
            tingkatan.kelas_count = self.env['efarina.kelas'].search_count([('tingkatan_id', '=', tingkatan.id)])

    pendidikan = fields.Selection([
        ('SMA', 'SMA'),
        ('SMK', 'SMK')
    ], string='Pendidikan', default='SMA', required=True)
    
    kewajiban_ids = fields.One2many('efarina.tingkatan.kewajiban', 'tingkatan_id', 'Biaya')
    kewajiban_count = fields.Integer('# Kewajiban', compute='_compute_kewajiban_count')

    kewajiban_checked_all = fields.Boolean('Checked All?', default=False, compute='_compute_checked_all')

    @api.depends('kewajiban_ids', 'kewajiban_ids.publish_data')
    def _compute_checked_all(self):
        for tingkatan in self:
            tingkatan.kewajiban_checked_all = all(tingkatan.kewajiban_ids.mapped('publish_data'))

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'The name of the tingkatan must be unique !')
    ]

    @api.depends('kewajiban_ids')
    def _compute_kewajiban_count(self):
        for tingkatan in self:
            tingkatan.kewajiban_count = self.env['efarina.tingkatan.kewajiban'].search_count([('tingkatan_id', '=', tingkatan.id)])

    def assign_all_kewajiban(self):
        if not self.kewajiban_ids:
            raise UserError(_("Kewajiban Masih Kosong!"))
        self.check_all_publish()
        self.assign_kewajiban()
        return True

    def check_all_publish(self):
        self.env['efarina.tingkatan.kewajiban'].search([('tingkatan_id', '=', self.id)]).write({
            'publish_data': True
        })
        return True

    def uncheck_all_publish(self):
        self.env['efarina.tingkatan.kewajiban'].search([('tingkatan_id', '=', self.id)]).write({
            'publish_data': False
        })
        return True

    def assign_kewajiban(self):
        if not self.kelas_ids:
            raise UserError(_("Harap Pilih Kelas terlebih dahulu!"))
        else:
            for kelas in self.kelas_ids:
                kelas.get_default_kewajiban()
        self.env['efarina.tingkatan.kewajiban'].search([('tingkatan_id', '=', self.id)]).write({
            'publish_data': False
        })
        return True

    def action_view_siswa(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('efarina.action_efarina_siswa')
        siswa = self.mapped('siswa_ids')

        if len(siswa) > 1:
            action['domain'] = [('id', 'in', siswa.ids)]

        elif siswa:
            form_view = [(self.env.ref('efarina.view_efarina_siswa_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = siswa.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action

    def action_view_kelas(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('efarina.action_efarina_kelas')
        kelas = self.mapped('kelas_ids')

        if len(kelas) > 1:
            action['domain'] = [('id', 'in', kelas.ids)]

        elif kelas:
            form_view = [(self.env.ref('efarina.view_efarina_kelas_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = kelas.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action

class Kewajiban(models.Model):
    _name = 'efarina.tingkatan.kewajiban'

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    publish_data = fields.Boolean('Publish?', required=True, default=False)
    tingkatan_id = fields.Many2one('efarina.tingkatan', 'Tingkatan', ondelete="cascade")
    name = fields.Char('Nama Kewajiban', required=True)
    biaya_id = fields.Many2one('efarina.biaya', 'Biaya', required=True)
    jumlah = fields.Monetary('Jumlah', default=0)
    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)
    type = fields.Selection(string="Jenis", related="biaya_id.type")

    @api.onchange('biaya_id')
    def _onchange_biaya(self):
        if self.biaya_id:
            self.name = self.biaya_id.name
            self.jumlah = self.biaya_id.jumlah