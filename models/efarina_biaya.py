# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging

_logger = logging.getLogger(__name__)

class Biaya(models.Model):
    _name = 'efarina.biaya'

    name = fields.Char('Nama Biaya', required=True)
    
    jenispendapatan_id = fields.Many2one('efarina.jenispendapatan', 'Jenis Pendapatan')
    def _get_default_currency_id(self):
        return self.env.company.currency_id.id
    
    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)
    type = fields.Selection([
        ('monthly', 'Monthly'),
        ('annualy', 'Annualy'),
        ('demand', 'Demand'),
        ('otc', 'One Time Charge')
    ],'Jenis Biaya', required=True, default="demand")
    jumlah = fields.Monetary('Jumlah', required=True, default=0)

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'The name of the biaya must be unique !')
    ]