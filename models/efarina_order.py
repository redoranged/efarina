# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging, datetime

_logger = logging.getLogger(__name__)

class Order(models.Model):
    _name = 'efarina.order'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'date_order desc'

    name = fields.Char(string='Order Reference', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))
    type = fields.Selection([
        ('in', 'IN'),
        ('out', 'OUT')
    ], 'Order Type', required=True, default="in", readonly=True, states={'draft': [('readonly', False)]})
    in_type = fields.Selection([
        ('murid', 'Murid'),
        ('lainnya', 'Lainnya')
    ], 'Tipe Pemasukan', default='murid', readonly=True, states={'draft': [('readonly', False)]})
    out_type = fields.Selection([
        ('operasional', 'Operasional'),
        ('non', 'Non Operasional')
    ], 'Tipe Pengeluaran', default='operasional', readonly=True, states={'draft': [('readonly', False)]})
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, tracking=True, default='draft')

    siswa_id = fields.Many2one('efarina.siswa', 'Siswa', readonly=True, states={'draft': [('readonly', False)]})
    origin = fields.Char(string='Source Document', help="Reference of the document that generated this order request.", tracking=True)

    date_order = fields.Datetime(string='Order Date', required=True, readonly=True, index=True, tracking=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, copy=False, default=fields.Datetime.now, help="Creation date of draft/sent orders,\nConfirmation date of confirmed orders.")
    nama = fields.Char('Nama')
    note = fields.Text('Keterangan', readonly=True, states={'draft': [('readonly', False)]})

    @api.onchange('type', 'in_type', 'siswa_id', 'jenispendapatan_id')
    def _onchange_nama(self):
        if self.type == 'in':
            if self.in_type == 'murid':
                nama = ''
                if self.siswa_id:
                    nama = self.siswa_id.name
                    if self.jenispendapatan_id:
                        nama = "{} ({})".format(nama, self.jenispendapatan_id.name)
                self.nama = nama
            else:
                if not self.jenispendapatan_id:
                    self.nama = False

    kewajiban_id = fields.Many2one('efarina.siswa.kewajiban', 'Kewajiban', readonly=True, states={'draft': [('readonly', False)]}, tracking=True)
    aset_id = fields.Many2one('efarina.aset', 'Bank or Cash', required=True, readonly=True, states={'draft': [('readonly', False)]})
    jumlah = fields.Monetary('Jumlah', readonly=True, states={'draft': [('readonly', False)]}, tracking=True)

    jenispendapatan_id = fields.Many2one('efarina.jenispendapatan', 'Jenis Pendapatan', readonly=True, states={'draft': [('readonly', False)]}, tracking=True)
    jenispengeluaran_id = fields.Many2one('efarina.jenispengeluaran', 'Jenis Pengeluaran', readonly=True, states={'draft': [('readonly', False)]}, tracking=True)
    parent_account_id = fields.Many2one('efarina.account', 'Category CoA')
    expense_account_id = fields.Many2one('efarina.account', 'CoA')
    
    @api.model
    def year_selection(self):
        year = 2015 # replace 2015 with your a start year
        year_list = []
        while year != 2035: # replace 2035 with your end year
            year_list.append((str(year), str(year)))
            year += 1
        return year_list

    filter_tahun = fields.Selection(
        year_selection,
        string="Filter Tahun",
        default=str(datetime.datetime.today().year), # as a default value it would be 2021
    )

    @api.onchange('filter_tahun')
    def _onchange_filter_tahun(self):
        self.kewajiban_id = False

    @api.onchange('kewajiban_id')
    def _onchange_kewajiban(self):
        if self.kewajiban_id:
            self.jenispendapatan_id = self.kewajiban_id.biaya_id.jenispendapatan_id

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)

    def action_confirm(self):
        self.state = 'confirmed'
        return True

    def action_done(self):
        self.state = 'done'
        return True   

    def action_set_draft(self):
        self.state = 'draft'
        return True

    def action_cancel(self):
        self.state = 'cancel'
        return True

    @api.model
    def create(self, values):
        if not values.get('name', False) or values['name'] == _('New'):
            if values['type'] == 'in':
                values['name'] = self.env['ir.sequence'].next_by_code('efarina.order.in') or _('New')
            elif values['type'] == 'out':
                values['name'] = self.env['ir.sequence'].next_by_code('efarina.order.out') or _('New')
            else:
                values['name'] = self.env['ir.sequence'].next_by_code('efarina.order') or _('New')

        res = super(Order, self).create(values)
        return res

    @api.onchange('kewajiban_id')
    def _onchange_kewajiban_id(self):
        if self.kewajiban_id:
            self.jumlah = self.kewajiban_id.sisa_harus_bayar

    @api.onchange('jenispengeluaran_id')
    def _onchange_jenispengeluaran_id(self):
        if self.jenispengeluaran_id:
            self.jumlah = self.jenispengeluaran_id.jumlah

    # Reporting

    # Kas Bulanan
    @api.model
    def report_ordermonth_wizard(self):
        """ Called by the 'Bulanan' button of the setup bar."""
        view_id = self.env.ref('efarina.laporan_ordermonth_wizard').id
        return {'type': 'ir.actions.act_window',
                'name': _('Laporan Pendapatan & Pengeluaran Bulanan'),
                'res_model': 'efarina.laporan.ordermonth',
                'target': 'new',
                'view_mode': 'form',
                'views': [[view_id, 'form']],
        }

    def export_ordermonth_xlsx(self, start_year, start_month, end_year, end_month):
        module = __name__.split("addons.")[1].split(".")[0]
        report_name = "{}.ordermonth_xlsx".format(module)

        report = {
            "type": "ir.actions.report",
            "report_type": "xlsx",
            "report_name": report_name,
            "model": 'efarina.order',
            # model name will be used if no report_file passed via context
            "context": dict(self.env.context, report_file="Laporan_KasBulanan_{}-{}_{}-{}".format(start_year, start_month, end_year, end_month), start_year=start_year, start_month=start_month, end_year=end_year, end_month=end_month),
            # report_xlsx doesn't pass the context if the data dict is empty
            # cf. report_xlsx\static\src\js\report\qwebactionmanager.js
            # TODO: create PR on report_xlsx to fix this
            "data": {"dynamic_report": True},
        }
        return report

    # Kas Harian
    @api.model
    def report_orderday_wizard(self):
        """ Called by the 'Harian' button of the setup bar."""
        view_id = self.env.ref('efarina.laporan_orderday_wizard').id
        return {'type': 'ir.actions.act_window',
                'name': _('Laporan Pendapatan & Pengeluaran Harian'),
                'res_model': 'efarina.laporan.orderday',
                'target': 'new',
                'view_mode': 'form',
                'views': [[view_id, 'form']],
        }

    def export_orderday_xlsx(self, date_start, date_end):
        module = __name__.split("addons.")[1].split(".")[0]
        report_name = "{}.orderday_xlsx".format(module)

        report = {
            "type": "ir.actions.report",
            "report_type": "xlsx",
            "report_name": report_name,
            "model": 'efarina.order',
            # model name will be used if no report_file passed via context
            "context": dict(self.env.context, report_file="Laporan_KasHarian_{}_{}".format(date_start, date_end), date_start=date_start, date_end=date_end),
            # report_xlsx doesn't pass the context if the data dict is empty
            # cf. report_xlsx\static\src\js\report\qwebactionmanager.js
            # TODO: create PR on report_xlsx to fix this
            "data": {"dynamic_report": True},
        }
        return report