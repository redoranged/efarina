# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging, datetime

_logger = logging.getLogger(__name__)

from odoo.exceptions import UserError

class Murid(models.Model):
    _name = 'efarina.siswa'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'name, id'

    name = fields.Char('Nama Siswa', required=True)
    kelas_id = fields.Many2one('efarina.kelas', 'Kelas', required=True, tracking=True)
    pendidikan = fields.Selection(string="Pendidikan", related='kelas_id.pendidikan', store=True)
    tingkatan_id = fields.Many2one('efarina.tingkatan', related='kelas_id.tingkatan_id', store=True, tracking=True)
    jurusan_id = fields.Many2one('efarina.jurusan', related='kelas_id.jurusan_id', store=True, tracking=True)
    tingkat = fields.Selection(string="Tingkat", related='kelas_id.tingkat', store=True, tracking=True)
    asrama_id = fields.Many2one('efarina.asrama', 'Asrama', tracking=True)
    nis = fields.Char('NIS', required=True, copy=False)
    ekstrakurikuler = fields.Char('Ekstrakurikuler')
    status = fields.Selection([
        ('bayar_penuh', 'Bayar Penuh'),
        ('tidak_bayar_penuh', 'Tidak Bayar Penuh'),
        ('tidak_bayar', 'Tidak Bayar')
    ], 'Status Siswa', default='bayar_penuh', required=True, copy=False, tracking=True)
    active = fields.Boolean('Active', default=True, help="If unchecked, it will allow you to hide the siswa without removing it.", tracking=True)
    agama = fields.Selection([
        ('islam', 'ISLAM'),
        ('protestan', 'PROTESTAN'),
        ('katolik', 'KATOLIK'),
        ('budha', 'BUDHA'),
        ('hindu', 'HINDU')
    ], 'Agama')
    alamat = fields.Text('Alamat')
    anak_ke = fields.Integer('Anak Ke')
    anak_dari = fields.Integer('Anak Dari')
    asal_sekolah = fields.Char('Asal Sekolah')
    berat_badan = fields.Float('Berat Badan', default=0)
    cita_cita = fields.Char('Cita-cita')
    email = fields.Char('Email')
    golongan_darah = fields.Char('Golongan Darah')
    hobby = fields.Char('Hobby')
    jenis_kelamin = fields.Selection([
        ('female', 'Female'),
        ('male', 'Male')
    ], 'Jenis Kelamin')
    no_hp = fields.Char('No HP')
    no_rekening = fields.Char('No Rekening')

    _sql_constraints = [
        ('nis_uniq', 'unique(nis)', "NIS hanya berlaku untuk 1 siswa !"),
    ]

    kewajiban_ids = fields.One2many('efarina.siswa.kewajiban', 'siswa_id', store=True)

    order_ids = fields.One2many('efarina.order', 'siswa_id', 'Order')
    order_count = fields.Integer('# Order',
        compute='_compute_order_count', compute_sudo=False, store=True)

    def get_default_kewajiban(self):
        if not self.kelas_id:
            raise UserError(_("Harap Pilih Kelas terlebih dahulu!"))
        else:
            for kewajiban in self.kelas_id.kewajiban_ids.filtered(lambda x: x.publish_data == True):
                exists = False
                domain = [
                    ('siswa_id', '=', self.id),
                    ('biaya_id', '=', kewajiban.biaya_id.id),
                ]
                if kewajiban.type == 'monthly':
                    domain.append(('month', '=', kewajiban.month))
                    domain.append(('year', '=', kewajiban.year))
                    
                if kewajiban.type == 'annualy':
                    domain.append(('year', '=', kewajiban.year))
                
                kewajiban_siswa = self.env['efarina.siswa.kewajiban'].search(domain)
                if len(kewajiban_siswa) > 0:
                    exists = True
                        
                if not exists:
                    self.env['efarina.siswa.kewajiban'].create({
                        'siswa_id': self.id,
                        'name': kewajiban.name,
                        'biaya_id': kewajiban.biaya_id.id,
                        'jumlah': kewajiban.jumlah,
                        'currency_id': kewajiban.currency_id.id,
                        'type': kewajiban.type,
                        'month': kewajiban.month,
                        'year': kewajiban.year,
                        'date': datetime.date.today(),
                        'due_date': kewajiban.due_date,
                    })
                else:
                    self.env.user.notify_info(message=_("%s sudah memiliki kewajiban %s") % (self.name, kewajiban.name))
                    _logger.info(_("%s sudah memiliki kewajiban %s") % (self.name, kewajiban.name))
        return True

    @api.depends('order_ids', 'order_ids.type', 'order_ids.state')
    def _compute_order_count(self):
        for siswa in self:
            siswa.order_count = self.env['efarina.order'].search_count([('siswa_id', '=', siswa.id)])

    def action_view_order(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('efarina.action_efarina_order')
        order = self.mapped('order_ids')

        if len(order) > 1:
            action['domain'] = [('id', 'in', order.ids)]

        elif order:
            form_view = [(self.env.ref('efarina.view_efarina_order_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = order.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action

    def action_new_order(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('efarina.action_efarina_order')
        order = self.mapped('order_ids')

        form_view = [(self.env.ref('efarina.view_efarina_order_form').id, 'form')]
        if 'views' in action:
            action['views'] = form_view + [
                (state, view) for state, view in action['views']
                if view != 'form']

        else:
            action['views'] = form_view

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False, default_siswa_id=self.id)

        return action

    
    # Reporting

    # Penagihan
    def report_penagihan_wizard(self):
        """ Called by the 'Print Penagihan' button of the setup bar."""
        view_id = self.env.ref('efarina.laporan_penagihan_wizard').id
        return {
            'type': 'ir.actions.act_window',
            'name': _('Laporan Penagihan'),
            'res_model': 'efarina.laporan.penagihan',
            'target': 'new',
            'view_mode': 'form',
            'views': [[view_id, 'form']],
            'context': {
                'active_model': 'efarina.siswa',
                'active_ids': self.ids,
                'default_siswa_id': self.id
            },
        }

    def export_penagihan_xlsx(self, semester, siswa_id, kelas_id, start_year, start_month, end_year, end_month, perihal, keterangan_bank):
        module = __name__.split("addons.")[1].split(".")[0]
        report_name = "{}.penagihan_xlsx".format(module)

        report = {
            "type": "ir.actions.report",
            "report_type": "xlsx",
            "report_name": report_name,
            "model": 'efarina.siswa',
            # model name will be used if no report_file passed via context
            "context": dict(self.env.context, report_file="Laporan_PenagihanSiswa_{}".format(siswa_id.name), semester=semester, siswa_id=siswa_id.id, kelas_id=kelas_id.id, start_year=start_year, start_month=start_month, end_year=end_year, end_month=end_month, perihal=perihal, keterangan_bank=keterangan_bank),
            # report_xlsx doesn't pass the context if the data dict is empty
            # cf. report_xlsx\static\src\js\report\qwebactionmanager.js
            # TODO: create PR on report_xlsx to fix this
            "data": {"dynamic_report": True},
        }
        return report
    
    # Resume Piutang
    @api.model
    def report_resume_wizard(self):
        """ Called by the 'Per Kelas' button of the setup bar."""
        view_id = self.env.ref('efarina.laporan_resume_wizard').id
        return {'type': 'ir.actions.act_window',
                'name': _('Laporan Resume Piutang'),
                'res_model': 'efarina.laporan.resume',
                'target': 'new',
                'view_mode': 'form',
                'views': [[view_id, 'form']],
        }

    def export_resume_xlsx(self, biaya_ids, start_year, start_month, end_year, end_month):
        module = __name__.split("addons.")[1].split(".")[0]
        report_name = "{}.resume_xlsx".format(module)

        report = {
            "type": "ir.actions.report",
            "report_type": "xlsx",
            "report_name": report_name,
            "model": 'efarina.siswa',
            # model name will be used if no report_file passed via context
            "context": dict(self.env.context, report_file="Laporan_Resume_{}-{}".format(end_year, end_month), biaya_ids=biaya_ids, start_year=start_year, start_month=start_month, end_year=end_year, end_month=end_month),
            # report_xlsx doesn't pass the context if the data dict is empty
            # cf. report_xlsx\static\src\js\report\qwebactionmanager.js
            # TODO: create PR on report_xlsx to fix this
            "data": {"dynamic_report": True},
        }
        return report

    # Informasi Siswa
    @api.model
    def report_informasisiswa_wizard(self):
        """ Called by the 'Informasi Siswa' button of the setup bar."""
        view_id = self.env.ref('efarina.laporan_informasisiswa_wizard').id
        return {'type': 'ir.actions.act_window',
                'name': _('Laporan Informasi Siswa'),
                'res_model': 'efarina.laporan.informasisiswa',
                'target': 'new',
                'view_mode': 'form',
                'views': [[view_id, 'form']],
        }

    def export_informasisiswa_xlsx(self):
        module = __name__.split("addons.")[1].split(".")[0]
        report_name = "{}.informasisiswa_xlsx".format(module)

        report = {
            "type": "ir.actions.report",
            "report_type": "xlsx",
            "report_name": report_name,
            "model": 'efarina.siswa',
            # model name will be used if no report_file passed via context
            "context": dict(self.env.context, report_file="Laporan_Informasi_Siswa"),
            # report_xlsx doesn't pass the context if the data dict is empty
            # cf. report_xlsx\static\src\js\report\qwebactionmanager.js
            # TODO: create PR on report_xlsx to fix this
            "data": {"dynamic_report": True},
        }
        return report

    # Penagihan Siswa
    @api.model
    def report_penagihansiswa_wizard(self):
        """ Called by the 'Penagihan Siswa' button of the setup bar."""
        view_id = self.env.ref('efarina.laporan_penagihansiswa_wizard').id
        return {'type': 'ir.actions.act_window',
                'name': _('Penagihan Siswa'),
                'res_model': 'efarina.laporan.penagihansiswa',
                'target': 'new',
                'view_mode': 'form',
                'views': [[view_id, 'form']],
        }

    def export_penagihansiswa_xlsx(self, semester, siswa_ids, start_year, start_month, end_year, end_month, perihal, keterangan_bank):
        module = __name__.split("addons.")[1].split(".")[0]
        report_name = "{}.penagihansiswa_xlsx".format(module)

        report = {
            "type": "ir.actions.report",
            "report_type": "xlsx",
            "report_name": report_name,
            "model": 'efarina.siswa',
            # model name will be used if no report_file passed via context
            "context": dict(self.env.context, report_file="Laporan_PenagihanSiswa_{}{}-{}{}".format(start_month,start_year,end_month,end_year), semester=semester, siswa_ids=siswa_ids, start_year=start_year, start_month=start_month, end_year=end_year, end_month=end_month, perihal=perihal, keterangan_bank=keterangan_bank),
            # report_xlsx doesn't pass the context if the data dict is empty
            # cf. report_xlsx\static\src\js\report\qwebactionmanager.js
            # TODO: create PR on report_xlsx to fix this
            "data": {"dynamic_report": True},
        }
        return report

    # Piutang Kelas
    @api.model
    def report_piutangkelas_wizard(self):
        """ Called by the 'Per Kelas' button of the setup bar."""
        view_id = self.env.ref('efarina.laporan_piutangkelas_wizard').id
        return {'type': 'ir.actions.act_window',
                'name': _('Laporan Piutang Per Kelas'),
                'res_model': 'efarina.laporan.piutangkelas',
                'target': 'new',
                'view_mode': 'form',
                'views': [[view_id, 'form']],
        }

    def export_piutangkelas_xlsx(self, kelas_ids, start_year, start_month, end_year, end_month):
        module = __name__.split("addons.")[1].split(".")[0]
        report_name = "{}.piutangkelas_xlsx".format(module)

        report = {
            "type": "ir.actions.report",
            "report_type": "xlsx",
            "report_name": report_name,
            "model": 'efarina.siswa',
            # model name will be used if no report_file passed via context
            "context": dict(self.env.context, report_file="Laporan_PiutangKelas_{}-{}_{}-{}".format(start_year, start_month, end_year, end_month ), kelas_ids=kelas_ids, start_year=start_year, start_month=start_month, end_year=end_year, end_month=end_month),
            # report_xlsx doesn't pass the context if the data dict is empty
            # cf. report_xlsx\static\src\js\report\qwebactionmanager.js
            # TODO: create PR on report_xlsx to fix this
            "data": {"dynamic_report": True},
        }
        return report
    
    # Piutang Bulanan
    @api.model
    def report_piutangbulanan_wizard(self):
        """ Called by the 'Bulanan' button of the setup bar."""
        view_id = self.env.ref('efarina.laporan_piutangbulanan_wizard').id
        return {'type': 'ir.actions.act_window',
                'name': _('Laporan Piutang Bulanan'),
                'res_model': 'efarina.laporan.piutangbulanan',
                'target': 'new',
                'view_mode': 'form',
                'views': [[view_id, 'form']],
        }

    def export_piutangbulanan_xlsx(self, year, month):
        module = __name__.split("addons.")[1].split(".")[0]
        report_name = "{}.piutangbulanan_xlsx".format(module)

        report = {
            "type": "ir.actions.report",
            "report_type": "xlsx",
            "report_name": report_name,
            "model": 'efarina.siswa',
            # model name will be used if no report_file passed via context
            "context": dict(self.env.context, report_file="Laporan_PiutangBulanan_{}-{}".format(year, month), year=year, month=month),
            # report_xlsx doesn't pass the context if the data dict is empty
            # cf. report_xlsx\static\src\js\report\qwebactionmanager.js
            # TODO: create PR on report_xlsx to fix this
            "data": {"dynamic_report": True},
        }
        return report

class Kewajiban(models.Model):
    _name = 'efarina.siswa.kewajiban'

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    siswa_id = fields.Many2one('efarina.siswa', 'Siswa', ondelete="cascade")
    name = fields.Char('Nama Kewajiban', required=True)
    biaya_id = fields.Many2one('efarina.biaya', 'Biaya', required=True)
    jumlah = fields.Monetary('Jumlah', default=0)
    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)
    type = fields.Selection(string="Jenis", related="biaya_id.type")
    date = fields.Datetime(string='Date', required=True, readonly=True, copy=False, index=True, default=fields.Datetime.now)
    due_date = fields.Date('Batas Tanggal Terakhir')

    @api.onchange('biaya_id')
    def _onchange_biaya(self):
        if self.biaya_id:
            self.name = self.biaya_id.name
            self.jumlah = self.biaya_id.jumlah
            
    @api.model
    def year_selection(self):
        year = 2015 # replace 2015 with your a start year
        year_list = []
        while year != 2035: # replace 2035 with your end year
            year_list.append((str(year), str(year)))
            year += 1
        return year_list

    year = fields.Selection(
        year_selection,
        string="Year",
        default="2021", # as a default value it would be 2021
    )

    month = fields.Selection([
        ('1', 'January'), 
        ('2', 'February'), 
        ('3', 'March'), 
        ('4', 'April'),
        ('5', 'May'), 
        ('6', 'June'), 
        ('7', 'July'), 
        ('8', 'August'), 
        ('9', 'September'), 
        ('10', 'October'), 
        ('11', 'November'), 
        ('12', 'December')
    ], 'Bulan')

    date_month_year = fields.Date('Date Month Year', compute="_compute_date_month_year", store=True)

    @api.depends('month', 'year')
    def _compute_date_month_year(self):
        for kewajiban in self:
            if kewajiban.month and kewajiban.year:
                kewajiban.date_month_year = datetime.date(int(kewajiban.year), int(kewajiban.month), 1)
            else:
                kewajiban.date_month_year = False

    jumlah_sudah_bayar = fields.Monetary('Sudah Bayar', compute="_compute_sisa", store=True)
    sisa_harus_bayar = fields.Monetary('Sisa', compute="_compute_sisa", store=True)

    order_ids = fields.One2many('efarina.order', 'kewajiban_id', 'Order')
    order_count = fields.Integer('# Order',
        compute='_compute_order_count', compute_sudo=False, store=True)

    @api.depends('order_ids', 'order_ids.type', 'order_ids.state')
    def _compute_order_count(self):
        for kewajiban in self:
            kewajiban.order_count = self.env['efarina.order'].search_count([('kewajiban_id', '=', kewajiban.id)])

    @api.depends('order_ids', 'order_ids.jumlah', 'order_ids.state', 'jumlah')
    def _compute_sisa(self):
        for kewajiban in self:
            kewajiban.jumlah_sudah_bayar = sum(kewajiban.order_ids.filtered(lambda x: x.type == 'in' and x.state in ['confirmed', 'done']).mapped('jumlah'))
            kewajiban.sisa_harus_bayar = kewajiban.jumlah - kewajiban.jumlah_sudah_bayar

    def name_get(self):
        bulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ]

        return [
            (kewajiban.id, 
                "%s %s" % 
                    (kewajiban.name, 
                        "%s %s" % 
                            (
                                "(%s)" % (bulan[int(kewajiban.month)-1]) if kewajiban.month and kewajiban.type == 'monthly' != False else ''
                            , 
                                "(%s)" % (kewajiban.year) if kewajiban.year and kewajiban.type in ['monthly', 'annualy'] != False else ''
                            )
                    )
            ) for kewajiban in self]