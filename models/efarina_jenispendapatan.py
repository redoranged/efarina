# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging

_logger = logging.getLogger(__name__)

class JenisPendapatan(models.Model):
    _name = 'efarina.jenispendapatan'

    name = fields.Char('Nama Jenis Pendapatan', required=True)

    detail_aset = fields.Boolean('Laporan Dengan Detail Aset?', default=True)

    order_ids = fields.One2many('efarina.order', 'jenispendapatan_id', 'Order')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'The name of the Jenis Pendapatan must be unique !')
    ]